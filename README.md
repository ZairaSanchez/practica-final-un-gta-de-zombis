# PRÁCTICA FINAL - Un GTA de zombis

## FUNCIONAMIENTO

En el juego se encarna a un personaje femenino que puede disparar y cuyo objetivo es matar a las zombis que se encuentre hasta que mate a la que tiene el reloj. Cuando lo haga y coja el reloj, habrá completado el nivel. Además, puede coger coches, conducirlos y atropellar a peatones y zombis.

## ESTRUCTURA

- Se ha creado un menú principal (Menu) al que se accede al empezar el juego. Para controlar su funcionamiento, se usa el script ControladorMenu. Tiene tres botones:
	· Nueva partida: inicia una partida.
	· Opciones: dirige a la pantalla de opciones (Menu_Opciones).
	· Salir: sale del juego.

- Se ha creado un menú de opciones (Menu_Opciones) al que se accede desde el menú principal. Para controlar su funcionamiento, se usa el script ControladorMenuOpciones. Tiene tres botones:
	· Controles: muestra los controles que se usan durante el juego (en el objeto ControlesOInstruciones).
	· Instrucciones: muestra las instrucciones del juego (en el objeto ControlesOInstruciones).
	· Menú principal: dirige al menú principal.

- Se ha creado un terrain sobre el que se ha colocado una ciudad con varios edificios, carreteras y árboles.

- Se ha creado una red de carreteras (ER Road Network) con 23 waypoints que sigue el trazado del terreno, por la que se desplazan los coches automáticos.

- Se han creado tres coches de tipo CarWaypointBased (Coche, Coche (1) y Coche (2)) que recorren la red de carreteras de forma autónoma. Además de tener las características de un coche autónomo, incluidos sus sonidos, la jugadora puede entrar en ellos y conducirlos, para lo que se les ha añadido los scripts ControladorJugador (para que se pueda pausar el juego mientras se conduce, por ejemplo), ControladorCoche (para que se pueda entrar y salir del coche, pulsando la tecla Intro/Return) y CarUserControl (para que se pueda manejar con los controles). Si la jugadora sale del coche, éste vuelve a ser automático y a seguir los waypoints de la carretera.

- Se han creado otros tres coches no automáticos (Car, Car (1) y Car (2)) y aparcados en la ciudad. Tienen las características de un coche de tipo Car, incluidos sus sonidos. Si la jugadora los coge, los puede manejar igual que los coches automáticos, ya que también tienen los scripts ControladorJugador (para que se pueda pausar el juego mientras se conduce, por ejemplo), ControladorCoche (para que se pueda entrar y salir del coche, pulsando la tecla Intro/Return) y CarUserControl (para que se pueda manejar con los controles).

- Al salir de un coche, éste se mantenía en movimiento. Se ha arreglado poniendo en el rigidbody del coche Is Kinematic a verdadero y las velocidades velocity y angularVelocity a 0.

- La cámara usada es de tipo FreeLookCameraRig, que sigue a la jugadora usando el script FreeLookCam, aunque si coge un coche, pasa a usar el script AutoCam para ponerse detrás del coche y seguirlo como se hace en los juegos de GTA.

- Se ha creado una jugadora en tercera persona con animaciones para caminar, girar, correr (en el blend tree Grounded), agacharse (en el blend tree Crouching), saltar y caer (en el blend tree Airborne), disparar (animación Shooting), ser golpeada (animación Head Hit) y morir (animación Falling Forward Death), para lo que se utiliza el Animator ThirdPersonAnimatorController. El control de la jugadora se realiza en el script ControladorJugador (vida, munición, armas, cambio de armas, Canvas, sonidos de recoger objetos o ser herida, sistema de partículas al ser herida y el proceso de recogida de objetos, coger o soltar coches, morir y ganar). Contiene un sistema de partículas que se dispara cuando recibe un golpe por parte de una zombi, y un audio source para que se oigan los disparos que realice con sus armas. El disparo se gestiona en el script Disparo (al disparar, como la animación es lenta, el sonido salía antes, con lo que se ha retrasado para que coincida en el tiempo con la animación).

- Al morir, el personaje se quedaba tumbado en el aire. Se ha solucionado cambiándole el Base upon a Feet del Root Transform Position (Y) de la animación. Se ha hecho lo mismo con el resto de las animaciones de las zombis y los peatones.

- Se han añadido una pistola y una metralleta con su correspondiente munición, adheridas a la mano de la jugadora, y controladas por el script ControladorArma. En este script y en Disparo, se han implementado sonidos, animaciones y sistemas de partículas para el disparo con y sin munición, el cambio de arma y la recarga. Además, se muestra por pantalla siempre el arma y la munición actuales. Las armas tienen un Audio Source para reproducir el sonido del disparo sin munición y usan el animator M1911 Handgun Controller y el sistema de partículas fireShot.

- En Enemigos se encuentran todos los enemigos del juego y utiliza el script GeneradorPersonajes para generar automáticamente las zombis. Cuando una muere, se genera otra en su lugar.
	· Se han creado zombis con las animaciones de caminar (animación Zombie walking), atacar (animaciones Zombie attack (ataque a la jugadora), Zombie Neck Bite (ataque a un peatón) y Zombie bitting (cuando la jugadora ha muerto)), recibir disparo (animación Grito), muriendo (cuando les queda uno de vida, animación Zombie casi muerto) y morir (animación Zombie death), en el Animator Zombie. Se crean zombis en posiciones aleatorias, de modo que cuando una muere, se crea otra, habiendo cinco zombis a la vez como máximo, además de los peatones convertidos en zombis (script GeneradorPersonajes).
	· Se han añadido zombis con otras animaciones en el Animator ZombieThriller (EnemigosPlataforma), pero como no he conseguido que se produzcan las transiciones entre animaciones, son meramente un elemento decorativo.
	· Las zombis tiene un Capsule Collider para recibir el daño de los disparos, y un Sphere Collider para detectar a la jugadora o a los peatones. Tienen un sistema de partículas que reproduce una explosión cuando recibe un disparo, y otro para cuando muere (más potente, Explosion), y un Nav Mesh Agent, que permite su movimiento aleatorio y errático.
	· El control de las zombis se hace en el script ControladorZombie (vida, animaciones y persecución y ataque a la jugadora y a los peatones (dejando tiempo entre ataques)). También se usa el script DeteccionJugador para detectar a la jugadora y a los peatones, y el script VidaZombie para gestionar el daño, la agonía y la muerte.

- Se puede saltar para llegar a una plataforma donde están las zombis que bailan Thriller.

- Se han repartido botiquines y cajas de munición por el escenario, que recargan la vida y la munición del arma que lleve en la mano la jugadora, respectivamente.

- Canvas:
	· En EstadoTPS, se muestra la vida de la jugadora (script EstadoTPS).
	· En MensajeMuerte, se muestran los mensajes "¡HAS MUERTO!" o "¡HAS GANADO!", dependiendo de si se ha perdido la vida o se ha completado el nivel.
	· En Arma se muestra la imagen del arma que la jugadora lleva en la mano.
	· En BarraMunicion, se muestra la munición del arma actual de la jugadora (script EstadoTPS).
	· Se ha creado un menú (Menu) para cuando se ha ganado la partida, se ha muerto o se pausa el juego (script ControladorMenu). Tiene tres botones: MenuPrincipal, que lleva al menú principal, Continuar, que continua la partida donde se pausó, y Salir, que cierra la aplicación.

- Se han creado diez peatones, cinco abuelitas y cinco personajes AJ, que utilizan GeneradorPersonajes de la misma forma que las zombis, cuando uno muere, se crea otro en una posición aleatoria.
	· Se ha creado un animator para las abuelitas (ControladorAbuelita), modificando el animator Zombie para que tenga nuevas animaciones: caminar (animación Female Walk), correr (cuando se acercan a una zombi, animación Running), morir (cuando una zombi les muerde, animación Dying) y convertirse en zombi (animación Zombie Transition).
	· Se ha creado un animator para los AJs (ControladorAJ), modificando el animator Zombie para que tenga nuevas animaciones: caminar (animación Dwarf Walk), correr (cuando se acercan a una zombi, animación Run), morir (cuando una zombi les muerde, animación Flying Back Death) y convertirse en zombi (animación Zombie Transition).
	· Al morder a un peatón y convertirse éste en zombi, había errores al no encontrar el controlador del peatón. Se ha arreglado permitiendo morderle una sola vez.
	· Se crean peatones en posiciones aleatorias, en el caso de que uno haya sido atropellado por un coche, habiendo cinco peatones de cada tipo a la vez como máximo (script GeneradorPersonajes).
	· Los peatones tienen un Capsule Collider para ser atropellados y recibir el daño de las zombis, y un Sphere Collider para detectarlas y huir de ellas. Tienen un sistema de partículas que reproduce una explosión cuando son atropellados (Explosion), y un Nav Mesh Agent, que permite su movimiento, girando 90º a la derecha cada 20 segundos.
	· El control de los peatones se hace en el script ControladorPeaton (vida, animaciones, explosión y conversión en zombi).
	
SCRIPTS:
- Contenido del script ControladorJugador:
	· Control del menú de pausa (tecla P). Si se pulsa P, aparece el menú de pausa, donde las opciones son ir al menú principal, continuar o salir del juego.
	· Cambio de arma (CambioArma): si se pulsa el botón secundario del ratón, se cambia de arma entre pistola y metralleta, tanto en la mano de la jugadora como en el Canvas, tanto la imagen como la munición. Suena el sonido de cambio de arma.
	· Gestión de entrar y salir de los coches (CogerCoche y DejarCoche): al coger un coche, los controles pasan a manejar el coche y la jugadora se oculta. Al dejar un coche, se invierte el proceso para volver a manejar a la jugadora con los controles.
	· Recogida de objetos (OnCollisionEnter): podemos coger un botiquín, que nos restaura la vida (RestauraVida), una caja de munición, que nos restaura la munición del arma que llevemos en la mano (RestauraMunicion), y el objeto ganador, que se trata del reloj que esconde una de las zombis y que necesitamos coger para completar el nivel (HasGanado). Al coger un objeto, éste se destruye y se oye un sonido; en el caso de la munición, se oye un sonido de recarga.
	· RestauraVida: actualiza la vida de la jugadora poniéndola al máximo y rellena la barra de vida del Canvas.
	· RestauraMunicion: actualiza la munición del arma que lleva en la mano la jugadora poniéndola al máximo y rellena la barra de munición del Canvas. Suena el sonido de recarga.
	· Gestión del daño recibido (RecibirDanyo): reproduce el sonido, la animación y el sistema de partículas de recibir un golpe. Reduce la vida de la jugadora y la barra de vida del Canvas. Si no nos queda vida, morimos (HasMuerto).
	· HasMuerto: reproduce la animación de muerte, y muestra un mensaje y el menú para salir del juego o al menú principal.
	· HasGanado: muestra un mensaje y el menú para salir del juego o al menú principal.
	
- Contenido del script ControladorCoche:
	· FixedUpdate: si estamos conduciendo un coche, podemos salir de él pulsando la tecla Intro/Return. Volvemos a controlar a la jugadora y a usar la FreeLookCam, y paramos el coche. Si éste es automático, volverá a ponerse en movimiento siguiendo los waypoints del circuito.
	· OnTriggerStay: al acercarse a un coche, la jugadora podrá entrar en él y conducirlo pulsando la tecla Intro/Return. Pasamos a controlar el coche y a usar la AutoCam, pero podemos usar los mismos controles que cuando manejamos a la jugadora. Si el coche es automático, desactivamos su inteligencia artificial mientras lo estemos conduciendo.
	
- Contenido del script Disparo:
	· Disparar: sonido (SonidoDisparo), animación, sistema de partículas del disparo y daño al enemigo (cada vez que se le dispara, recibe 1 de daño).
	· SonidoDisparo: reproduce el sonido del disparo para que coincida en el tiempo con la animación.
	
- Contenido del script ControladorArma:
	· GestionarDisparo: tenemos una munición de 50 al inicio, y si pulsamos el botón izquierdo del ratón, la dispararemos. En el caso de la pistola, sólo podemos disparar una bala a la vez, mientras que con la metralleta, podemos hacer ráfagas de disparos. Al producirse el disparo, se actualiza la munición del arma y en el Canvas, además de producirse la animación, el sonido y el sistema de partículas del disparo, y, si alcanzamos a una zombi, disminuiremos en uno su vida. Si no nos queda munición, no podremos disparar.
	· Recarga: si cogemos una caja de munición, el arma que tenemos en la mano se recargará, actualizando la cantidad de munición también en el Canvas y produciéndose un sonido de recarga.
	· CambioArma: sonido del cambio de arma.
	
- Contenido del script GeneradorPersonajes:
	· Cuando hay menos de 5 zombis o peatones, porque éstos hayan muerto, se generan otros en una posición aleatoria del mapa.
	
- Contenido del script ControladorZombie:
	· DeteccionJugador_enDeteccion: el objetivo es la jugadora  o los peatones (DeteccionJugador).
	· FixedUpdate: si la zombi está viva, se mueve por puntos aleatorios, a menos que haya detectado a la jugadora o a un peatón, en cuyo caso, los persigue. Si está lo suficientemente cerca del objetivo, lo golpea o lo muerde.
	· OnTriggerEnter: si el Capsule Collider de una zombi es alcanzado por el frontal de un coche, muere atropellada con una explosión.
	· OnTriggerStay: tiene que pasar cierto tiempo entre ataque y ataque. La zombi ataca a la jugadora sólo cuando la tiene muy cerca, reproduciéndose la animación de ataque. Con los peatones hace lo mismo, cuando los tiene muy cerca, los muerde.
	· OnTriggerExit: si la jugadora se aleja, la zombi deja de perseguirla y atacarla. También se contempla la posibilidad de que un coche se aleje sin haber atropellado a la zombi.
	· DanyaJugador: cada vez que una zombi golpea a la jugadora, ésta recibe uno de daño. Cuando la jugadora muere, la zombi se tira al suelo a morderla.
	· DePeatonAZombi: si una zombi muerde a un peatón, éste muere, desaparece y aparece en su lugar una zombi.

- Contenido del script DeteccionJugador:
	· OnTriggerEnter: si detecta a la jugadora o a un peatón, lo persigue.
	
- Contenido del script VidaZombie:
	· RecibirDisparo: disminuye la vida de la zombi y ésta grita. Si le queda uno de vida, agoniza, y si no tiene vida, muere.
	· Gritar: la zombi hace la animación de gritar y se reproduce el sistema de partículas de explosión.
	· Agonizar: animación de agonizar.
	· Morir: animación de morir.
	· Desaparecer: pasados unos segundos después de morir, la zombi desaparece y, si tiene el reloj, lo suelta.
	· MorirAtropellado: se reproduce el sistema de partículas de explosión al morir atropellada una zombi.
	· DesaparecerExplosion: pasados unos segundos después de morir, la zombi desaparece y, si tiene el reloj, lo suelta.
	· DesaparicionZombi: la zombi desaparece y, si tiene el reloj, lo suelta.
	· Vida: devuelve la vida actual de la zombi.
	
- Contenido del script EstadoTPS (gestiona las barras de vida y municiones):
	· GestionaBarra: actualiza el nivel de la barra pasado por parámetro.
	· ActualizaVida: actualiza el nivel de vida.
	· ActivarSombraVida: activa la sombra de la barra de vida.
	· ActualizaMunicionPistola: actualiza el nivel de munición de la pistola.
	· ActivarSombraMunicionPistola: activa la sombra de la barra de munición de la pistola.
	· ActualizaMunicionMetralleta: actualiza el nivel de munición de la metralleta.
	· ActivarSombraMunicionMetralleta: activa la sombra de la barra de munición de la metralleta.

- Contenido del script ControladorMenu:
	· NuevaPartida: carga el nivel.
	· MenuPrincipal: abre el menú principal.
	· ContinuaPartida: continúa la partida donde se pausó.
	· MenuOpciones: abre el menú de opciones.
	· Salir: cierra el juego.

- Contenido del script ControladorMenuOpciones:
	· Controles: muestra los controles por pantalla.
	· Instrucciones: muestra las instrucciones del juego por pantalla.
	· MenuPrincipal: vuelve al menú principal.
	
- Contenido del script ControladorPeaton:
	· FixedUpdate: cada 20 segundos, rotamos al peatón 90 grados.
	· RotarPeaton: hace la rotación del peatón 90 grados.
	· OnTriggerEnter: cuando se acerca a una zombi, el peatón corre. Si es atropellado por un coche, explota y desaparece.
	· OnTriggerExit: si se aleja de una zombi, deja de correr. También se contempla la posibilidad de que un coche se aleje sin haber atropellado al peatón.
	· MorirAtropellado: si el peatón es atropellado, se reproduce el sistema de partículas de la explosión y desaparece.
	· DesaparecerExplosion: dos segundos después de explotar, el peatón desaparece.
	· OnDisable: si ha muerto por mordedura de zombi, se crea una zombi en su lugar.

